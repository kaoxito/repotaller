function getClientes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            console.log(request.responseText)
            clientesObtenidos = request.responseText;
            procesarClientes(clientesObtenidos);
        }
    };
    request.open("GET",url, true);
    request.send();
}

function procesarClientes(datos) {
    var jsonProductos = JSON.parse(datos);
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");
 
    jsonProductos.value.forEach(function(element, index) {

        console.log(element.ProductName)
        var nuevaFila = document.createElement("tr");
        var columnaId = document.createElement("td");
        columnaId.innerText = element.CustomerID;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = element.ContactName;
        var columnaBandera = document.createElement("td")
        var bandera = element.Country == 'UK' ? 'United-Kingdom' : element.Country
        columnaBandera.innerHTML = "<img src=\"http://www.countries-ofthe-world.com/flags-normal/flag-of-"+bandera+".png\" ></img>";
        columnaBandera.classList.add("flag");

        nuevaFila.appendChild(columnaId);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaBandera)

        tbody.appendChild(nuevaFila)

    });

    tabla.appendChild(tbody)
    divTabla.appendChild(tabla)

}