function getProductos() {
    var productosObtenidos;
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            console.log(request.responseText)
            productosObtenidos = request.responseText;
            procesarProductos(productosObtenidos);
        }
    };
    request.open("GET",url, true);
    request.send();
}

function procesarProductos(datos) {
    var jsonProductos = JSON.parse(datos);
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    //console.log(jsonProductos.value[0].ProductName);
    // for (let i = 0; i < jsonProductos.value.length; i++) {
    //     console.log(jsonProductos.value[i].ProductName)
    //     var nuevaFila = document.createElement("tr");
    //     var columnaNombre = document.createElement("td");
    //     columnaNombre.innerText = jsonProductos.value[i].ProductName;
    //     var columnaPrecio = document.createElement("td");
    //     columnaPrecio.innerText = jsonProductos.value[i].UnitPrice;
    //     var columnaStock = document.createElement("td")
    //     columnaStock.innerText = jsonProductos.value[i].UnitsInStock;

    //     nuevaFila.appendChild(columnaNombre);
    //     nuevaFila.appendChild(columnaPrecio);
    //     nuevaFila.appendChild(columnaStock)

    //     tbody.appendChild(nuevaFila)

    // }

    
    jsonProductos.value.forEach(function(element, index) {

        console.log(element.ProductName)
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = element.ProductName;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = element.UnitPrice;
        var columnaStock = document.createElement("td")
        columnaStock.innerText = element.UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock)

        tbody.appendChild(nuevaFila)

    });

    tabla.appendChild(tbody)
    divTabla.appendChild(tabla)

}